<?php

class views_plugin_row_node_withfields extends views_plugin_row_node_view{
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $options = array('' => t('- None -'));
    $field_labels = $this->display->handler->get_field_labels();
    $options += $field_labels;
    
    $form['view_mode_set']=array(
      '#type'=>'fieldset',
      '#title'=>'Display Mode',
    );
    
    $form['view_mode_set']['display_type']=array(
        '#type'=>"radios",
        '#default_value'=>$this->options['view_mode_set']['display_type'],
        '#options'=>array(
            'fixed'=>"Fixed Display",
            'field'=>"Derived from a field",
        )
    );
    
    $form['view_mode_set']['view_mode_field'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#title' => t('Field To Use'),
        '#default_value' => $this->options['view_mode_set']['view_mode_field'],
        '#states' => array(
            'visible' => array(
                ':input[name="row_options[view_mode_set][display_type]"]' => array('value' => 'field'),
            ),
        ),
    );
    $form['view_mode']['#default_value']=$this->options['view_mode_set']['view_mode'];
    $form['view_mode']['#states'] = array(
        'visible' => array(
        ':input[name="row_options[view_mode_set][display_type]"]' => array('value' => 'fixed'),
      )
    );     
    $form['view_mode_set']['view_mode']=$form['view_mode'];
    unset($form['view_mode']);
  }
  
  function render($row) {
    if($this->options['view_mode_set']['display_type']=='field'){
      $display_mode=$this->display->handler->handlers['field'][$this->options['view_mode_set']['view_mode_field']]->field_alias;
    }
    else{
      $display_mode=$this->options['view_mode_set']['view_mode'];
    }
    
    $node = $this->nodes[$row->{$this->field_alias}];
    $node->view = $this->view;
    $build = node_view($node, $display_mode);

    return drupal_render($build);
  }

  function summary_title() {
    if($this->options['view_mode_set']['display_type']=='field'){
      return check_plain(t('Field Display Source: !field', array('!field'=>$this->options['view_mode_set']['view_mode_field'])));
    }
    else{
      $options = $this->options_form_summary_options();
      return check_plain($options[$this->options['view_mode_set']['view_mode']]);
    }
  }
}